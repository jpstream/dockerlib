all: dockerlib

dockerlib: prepare
	rm -rf dockerlib/__pycache__
	python3 -m py_compile dockerlib/*

install: dockerlib
	rm -rf dockerlib/__pycache__ /usr/share/dockerlib
	mkdir /usr/share/dockerlib
	cp dockerlib/* /usr/share/dockerlib
	cp -r data /usr/share/dockerlib
	echo "#!/bin/bash" > /usr/bin/dockerlib
	echo 'python3 /usr/share/dockerlib/main.py $$@' >> /usr/bin/dockerlib
	chmod +x /usr/bin/dockerlib

prepare:
	pip3 install -r requirements.txt

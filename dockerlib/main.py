import sys
import helper
import build

# Get Arguments passed
args = sys.argv[1:]

# If a argument passed, run:
if len(args) >= 1:

    # Build the Image
    if args[0] == "build":

        # If second argument passed, run build function
        if len(args) >= 2:
            build.run(args[1])
        else:
            helper.error("No argument for build image passed", 1)
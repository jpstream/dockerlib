import platform
import helper

# Replace placeholder with real strings

def replace(string, image):
    output = string

    # If architecutre placeholder exist, replace them
    if "%arch" in output:
        output = output.replace("%arch", replace_architecture(image))

    # If version placeholder exist, replace them
    if "%version" in output:
        output = output.replace("%version", replace_version(image))

    return output

# Replace Architecture with uname and read arch aliases from image
def replace_architecture(image):
    uname = platform.uname().machine
    arch = uname

    # Read if architecure alias is set and replace to `arch`-var.
    if "architectures" in image:
        if uname in image["architectures"]:
            arch = image["architectures"][uname]
    
    return arch

def replace_version(image):
    BUILD_VERSION_ENV_KEY="build_version"
    BUILD_VERSION_IMAGE_DEFAULT_KEY="default_build_version"

    # If a default version is given by image config use set this as `or` for `env0r`.
    # If image key not set require to set env variable
    if BUILD_VERSION_IMAGE_DEFAULT_KEY in image:
        return helper.env0r(BUILD_VERSION_ENV_KEY, image[BUILD_VERSION_IMAGE_DEFAULT_KEY])
    else:
        return helper.env_required(BUILD_VERSION_ENV_KEY)
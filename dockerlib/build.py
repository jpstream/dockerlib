import os
import helper
import load
import placeholder

DATA_FOLDER=helper.env0r("data_folder", os.getcwd() + "/data")

def run(image_name):

    # Load image config in variable
    image = load.image(DATA_FOLDER, image_name)

    # Define temp direcory and the workdir which will be removed and new created
    TEMP_FOLDER = helper.env0r("temp_folder", "/tmp")
    WORKDIR = f"{TEMP_FOLDER}/dockerlib/{image_name}"

    # If Directory exists, check if .git folder exists and put result to variable and create directory if needed
    if os.path.exists(WORKDIR):
        if os.path.exists(WORKDIR + "/.git"):
            git_pull=True
        else:
            git_pull=False
    else:
        git_pull=False
        os.makedirs(WORKDIR)

    # Clone git repository and set work directory to repo
    if "git" in image:

        # Define variables that has only will set if the parent class includes them.
        REPO = placeholder.replace(image["git"]["repo"], image)
        if "branch" in image["git"]:
            BRANCH = placeholder.replace(image["git"]["branch"], image)
        if "folder" in image["git"]:
            FOLDER = placeholder.replace(image["git"]["folder"], image)

        # If .git Directory won't exist, clone repository
        if not git_pull:
            # Clone the git repository in the workdir
            os.system(f"""
                git clone {REPO} {WORKDIR}
            """)

        WORKDIR = f"{TEMP_FOLDER}/dockerlib/{image_name}"

        # Checkout a specific branch if wished
        if "branch" in image["git"]:
            os.system(f"""
                cd {WORKDIR}
                git checkout {BRANCH}
            """)

        if git_pull:
            # Pull repository if folder is existing
            os.system(f"""
                cd {WORKDIR}
                git config pull.rebase false
                git pull
            """)

        # Append folder name to workdir if wished
        if "folder" in image["git"]:
            WORKDIR = WORKDIR + "/" + FOLDER

    # Run some Docker functions (currently only build of docker)
    if "docker" in image:

        # Build Docker Image
        if "build" in image["docker"]:

            COMMAND_ARGS = ""

            # Add the dockerfile path to command args if wished
            if "file" in image["docker"]["build"]:
                COMMAND_ARGS = f"{COMMAND_ARGS} --file {WORKDIR}/{image['docker']['build']['file']}"

            # Add tags for the new docker image into the command args if wished
            if "tags" in image["docker"]["build"]:
                for tag in image["docker"]["build"]["tags"]:
                    COMMAND_ARGS = f"{COMMAND_ARGS} -t {placeholder.replace(tag, image)}"
            
            # Build the docker image with the final command args
            os.system(f"""
               docker build {WORKDIR} {COMMAND_ARGS}
            """)
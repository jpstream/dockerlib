## dockerlib@1.1

- Support for Images in Subdirecories of Datafolder (https://codeberg.org/fossdd/dockerlib/commit/07e70cb108b4d7a18fd3b289796b73d06074cbec)
- Support to create a own file for a tag. (https://codeberg.org/fossdd/dockerlib/commit/87442481a123548925d3708d7c78b770c73a1047)
- Only pull git repository if not already existing (https://codeberg.org/fossdd/dockerlib/commit/01d734dd5367f85caa7d380791eb2061f9d8f878)
- New Images
  - `nginx` and `nginx:stable` (https://codeberg.org/fossdd/dockerlib/commit/54c6b1fb533535ef55317d5f2a53985d7e9f8373)
  - `archlinux` and `archlinux:base-devel` (https://codeberg.org/fossdd/dockerlib/commit/20eb7f17af0aa0efc9163df673daf5909eee4997)
- Bug fixes
  - Printing Error Messages to Console (https://codeberg.org/fossdd/dockerlib/commit/a83fa8c41a21547b013d1f53971f0ea6d8b8bf2b, https://codeberg.org/fossdd/dockerlib/commit/f8623cfbcb0d987d53ce3a06c65ee955c53d8d76, https://codeberg.org/fossdd/dockerlib/commit/75df49af568d9718b2173a7ed25b201a208ed442)
  - Missing `toml`-Requirement if installing like the guide (https://codeberg.org/fossdd/dockerlib/commit/199493e275b2570a8d54b61a9a63c6ed4081e495)
- Error if no build image argument passed (https://codeberg.org/fossdd/dockerlib/commit/abc3a1b6e674bb3a22f0f13c3118767837540182)
- Improve Source Code (https://codeberg.org/fossdd/dockerlib/commit/6bd9a785fe5414c9d48b5b055c015d886665bed0, https://codeberg.org/fossdd/dockerlib/commit/6b452c429ddb799b22514bdef64f428687b722a5, https://codeberg.org/fossdd/dockerlib/commit/e02e1581c30e94d290340c08d085c20e315bb258)